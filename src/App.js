import React from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import './App.css';

const initialValues = {
  firstName: '',
  lastName: '',
  age: '',
  employed: false,
  favoriteColor: '',
  sauces: [],
  bestStooge: '',
  notes: '',
};

const validationSchema = Yup.object().shape({
  firstName: Yup.string().matches(/^[A-Za-z ]*$/, 'Invalid characters').required('Required'),
  lastName: Yup.string().matches(/^[A-Za-z ]*$/, 'Invalid characters').required('Required'),
  age: Yup.number().integer('Invalid number').required('Required'),
  favoriteColor: Yup.string().required('Required'),
  sauces: Yup.array().min(1, 'Select at least one sauce'),
  notes: Yup.string().max(100, 'Too long'),
});

const UserForm = () => {
  return (
    <div className='center-form'>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values) => {
          alert(JSON.stringify(values, null, 2));
        }}
      >
        {({ isSubmitting, resetForm }) => (
          <Form>
            <div className="form-group">
              <label htmlFor="firstName">First Name</label>
              <Field type="text" id="firstName" name="firstName" />
              <ErrorMessage name="firstName" component="div" className="error" />
            </div>

            <div className="form-group">
              <label htmlFor="lastName">Last Name</label>
              <Field type="text" id="lastName" name="lastName" />
              <ErrorMessage name="lastName" component="div" className="error" />
            </div>

            <div className="form-group">
              <label htmlFor="age">Age</label>
              <Field type="number" id="age" name="age" />
              <ErrorMessage name="age" component="div" className="error" />
            </div>

            <div className="form-group">
              <label>Employed</label>
              <Field type="checkbox" name="employed" />
            </div>

            <div className="form-group">
              <label htmlFor="favoriteColor">Favorite Color</label>
              <Field as="select" id="favoriteColor" name="favoriteColor">
                <option value="">Select a color</option>
                <option value="Red">Red</option>
                <option value="Blue">Blue</option>
                <option value="Green">Green</option>
                <option value="Yellow">Yellow</option>
              </Field>
              <ErrorMessage name="favoriteColor" component="div" className="error" />
            </div>

            <div className="form-group">
              <label>Sauce Choices</label>
              <div className='custom-checkbox-div'>
                <label className="custom-checkbox">
                  <Field type="checkbox" name="sauces" value="Ketchup" />
                  Ketchup
                </label>
                <label className="custom-checkbox">
                  <Field type="checkbox" name="sauces" value="Mustard" />
                  Mustard
                </label>
                <label className="custom-checkbox">
                  <Field type="checkbox" name="sauces" value="Mayonnaise" />
                  Mayonnaise
                </label>
                <label className="custom-checkbox">
                  <Field type="checkbox" name="sauces" value="Guacamole" />
                  Guacamole
                </label>
              </div>
              <ErrorMessage name="sauces" component="div" className="error" />
            </div>

            <div className="form-group">
              <label>Best Stooge</label>
              <div className='form-group-colomb'>
                <label>
                  <Field type="radio" name="bestStooge" value="Larry" /> Larry
                </label>
                <label>
                  <Field type="radio" name="bestStooge" value="Moe" /> Moe
                </label>
                <label>
                  <Field type="radio" name="bestStooge" value="Curly" /> Curly
                </label>
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="notes">Notes</label>
              <Field as="textarea" id="notes" name="notes" />
              <ErrorMessage name="notes" component="div" className="error" />
            </div>

            <div className="button-group">
              <button type="submit" disabled={isSubmitting}>Submit</button>
              <button className="reset-btn" type="button" onClick={() => resetForm()} disabled={isSubmitting}>Reset</button>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default UserForm;
